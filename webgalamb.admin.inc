<?php

/**
 * @file
 * Administration section of the Webgalamb module.
 */

/**
 * The Webgalamb settings form.
 *
 * @todo Validate the given value.
 */
function webgalamb_admin_settings() {
  $form['webgalamb_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of the Webgalamb instance (end it with a "/")'),
    '#description' => t('Enter the full URL of the Webgalamb instance that you would like to use. The value should end with a /.'),
    '#default_value' => variable_get('webgalamb_url', 'http://'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return system_settings_form($form);
}
